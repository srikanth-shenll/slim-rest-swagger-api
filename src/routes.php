<?php
// Routes

//get airline details
$app->get('/airline', 'AirlineController:getAirlineAction');

//add airline details
$app->post('/airline/add', 'AirlineController:addAirlineAction');

//update airline details
$app->put('/airline/update/{id}', 'AirlineController:updateAirlineAction');

//delete airline details
$app->delete('/airline/delete/{id}', 'AirlineController:deleteAirlineAction');

