<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};


// Database
$container['db'] = function ($c) {
    $settings = $c->get('settings')['db'];
    $username = $settings['username'];
    $password = $settings['password'];
    $host	  = $settings['host'];
    $dbname   = $settings['dbname'];
    try{
    	$options = array(
		    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
		); 
    	$connection = new PDO('mysql:host='.$host.';dbname='.$dbname, $username, $password, $options);
	}catch(PDOException $e){
		$connection = $e->getMessage();
	}
    return $connection ;
};
