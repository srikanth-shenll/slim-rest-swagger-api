<?php

namespace App\Models;

/**
 * @SWG\Definition(required={}, @SWG\Xml(name="Airline"))
 */

Class AirlineModel{

	protected $db;
	public $table;
	
	/**
	* @SWG\Property(format="int64")
	* @var int
	*/
	public $id;

	/**
	* @SWG\Property
	* @var string
	*/
	public $airline_name;

	/**
	* @SWG\Property
	* @var string
	*/
	public $airline_code;

	public function __construct($db){

		$this->db = $db;
		$this->table = 'airlines_list';
	}

	public function getAirlines(){
		$query = 'Select * from '.$this->table;
		return $this->makeAirlineResponse($query);
	}

	public function getAirlineById($id){

		$query = 'Select * from '.$this->table.' where id = :id';
		return $this->makeAirlineResponse($query, array(':id' => $id));
		
	}

	public function getAirlineByName($name){
		$query = 'Select * from '.$this->table.' where airline_name = :name';
		return $this->makeAirlineResponse($query, array(':name' => $name));
	}

	public function getAirlineByCode($code){
		$query = 'Select * from '.$this->table.' where airline_code = :code';
		return $this->makeAirlineResponse($query, array(':code' => $code));
	}

	public function getAirlineByParams($getParams){
		$query = 'Select * from '.$this->table;
		$query .= ' where '; 
		$bindParams = array();
		$addCond = false;

		if(array_key_exists('name', $getParams)){
			$query .= 'airline_name = :name';
			$bindParams[':name'] = $getParams['name'];
			$addCond = true;
		}
		if(array_key_exists('code', $getParams)){
			$query .= ($addCond)? ' and airline_code = :code' : 'airline_code = :code';
			$bindParams[':code'] = $getParams['code'];
			$addCond = true;
		}
		if(array_key_exists('id', $getParams)){
			$query .= ($addCond)? ' and id = :id' : 'id = :id';
			$bindParams[':id'] = $getParams['id'];
		}

		return $this->makeAirlineResponse($query, $bindParams);
	}

	public function makeAirlineResponse($query, $params = array()){
		$getParams = array();
		$airlineList = array();
		$airlineListStmt = $this->db->prepare($query);
		$airlineListStmt->execute($params);

		if($airlineListStmt->rowCount() > 0){
			while($eachRow  = $airlineListStmt->fetch(\PDO::FETCH_ASSOC)){
				$airline = [];
				$airline['name'] = $eachRow['airline_name'];
				$airline['code'] = $eachRow['airline_code'];
				$airline['id']   = $eachRow['id'];

				$airlineList[] = $airline;
			}
		}
		return $airlineList;
	}

}
