<?php

namespace App\Controllers;
use App\Models;

Class AirlineController{

	protected $db;
	protected $model;

	public function __construct($db){

		$this->db = $db;
		$this->model = new \App\Models\AirlineModel($db);
	}

	/**
	*
	* @SWG\Get(
	*     path="/airline",
	*     summary="Find and Get Airline details",
	*     tags={"airline"},
	*     description="Muliple tags can be provided with comma seperated strings. Use tag1, tag2, tag3 for testing.",
	*     operationId="findAirline",
	*     consumes={"application/json"},
	*     produces={"application/json"},
	*     @SWG\Parameter(
	*         name="id",
	*         in="query",
	*         description="Filter airlines based on airline id",
	*         required=false,
	*         type="integer",
	*     ),
	*     @SWG\Parameter(
	*         name="name",
	*         in="query",
	*         description="Filter airlines based on airline name",
	*         required=false,
	*         type="string",
	*     ),
	*     @SWG\Parameter(
	*         name="code",
	*         in="query",
	*         description="Filter airlines based on airline code",
	*         required=false,
	*         type="string",
	*     ),
	*     @SWG\Response(
	*         response=200,
	*         description="successful operation",
	*         @SWG\Schema(
	*			type="array",
	*			@SWG\Items(ref="#/definitions/AirlineModel")
	*         ),
	*     ),
	*     @SWG\Response(
	*         response="400",
	*         description="Ivalid operation",
	*     )
	* )
	*/
	
	public function getAirlineAction($request, $response, $args){
		// success - 200
		// error - 404

		/**Allowed Request paramters
		/* id, name, code
		/* Response parameters
		/* status - string, data - array, total - integer
		*/
		$allowedParams = array('name', 'code', 'id');
		$airlineList = array();
		$allGetVars = $_GET;
		$getParams = array_intersect_key($allGetVars, array_flip($allowedParams));
		if(empty($getParams)){
			$airlineList = $this->model->getAirlines();
		}else{
			if(count($getParams) == 1){
				if(isset($getParams['id'])){
					$airlineList = $this->model->getAirlineById($getParams['id']);
				}elseif(isset($getParams['name'])){
					$airlineList = $this->model->getAirlineByName($getParams['name']);
				}elseif(isset($getParams['code'])){
					$airlineList = $this->model->getAirlineByCode($getParams['code']);
				}
			}else{
				$airlineList = $this->model->getAirlineByParams($getParams);
			}
		}
		
		$returnResponse = array('status' => 'ok', 'data' => $airlineList, 'total' => count($airlineList));
        $newResponse = $response->withJson($returnResponse, 200);
		return $newResponse;
	}

	/**
	*
	* @SWG\Post(
	*     path="/add",
	*     summary="Find and Get Airline details",
	*     tags={"airline"},
	*     description="Add new airline details to the system",
	*     operationId="addAirline",
	*     consumes={"application/json"},
	*     produces={"application/json"},
	*     @SWG\Parameter(
	*         name="id",
	*         in="query",
	*         description="Airline id to be integer",
	*         required=true,
	*         type="integer",
	*     ),
	*     @SWG\Parameter(
	*         name="name",
	*         in="query",
	*         description="Name of Airline",
	*         required=true,
	*         type="string",
	*     ),
	*     @SWG\Parameter(
	*         name="code",
	*         in="query",
	*         description="Airline code",
	*         required=true,
	*         type="string",
	*     ),
	*     @SWG\Response(
	*         response=200,
	*         description="Airline added successful",
	*         @SWG\Schema(
	*             type="array",
	*             @SWG\Items(ref="#/definitions/Pet")
	*         ),
	*     ),
	*     @SWG\Response(
	*         response="400",
	*         description="Ivalid operation",
	*     )
	* )
	*/
	public function addAirlineAction($request, $response, $args){
		// success - 201
		// error - 404, 409

		$returnResponse = array('status' => 'ok', 'data' => 'Added new airline');
        $newResponse = $response->withJson($returnResponse, 200);
		return $response;
	}

	public function updateAirlineAction($request, $response, $args){
		// success - 200
		// error - 404

		$returnResponse = array('status' => 'ok', 'data' => 'updated airline details');
        $newResponse = $response->withJson($returnResponse, 200);
		return $response;
	}

	public function deleteAirlineAction($request, $response, $args){
		// success - 200
		// error - 404

		$returnResponse = array('status' => 'ok', 'data' => 'Deleted airline');
        $newResponse = $response->withJson($returnResponse, 200);
		return $response;
	}
}
